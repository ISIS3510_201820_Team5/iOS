//
//  FeedListCell.swift
//  Pina
//
//  Created by Santiago Rojas on 10/27/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class FeedListCell: UICollectionViewCell {
    static let identifier = "FeedListCell"

    @IBOutlet weak var listNameLabel: UILabel!
    
    override func awakeFromNib() {
        clipsToBounds = true
        autoresizesSubviews = true
        
        layer.cornerRadius = 10
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 10)
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        layer.shadowOpacity = 0.5
        layer.shadowPath = shadowPath.cgPath
    }
    
    override var isHighlighted: Bool {
        didSet {
            let duration = isHighlighted ? 0.45 : 0.4
            let transform = isHighlighted ?
                CGAffineTransform(scaleX: 0.96, y: 0.96) : CGAffineTransform.identity
            UIView.animate(withDuration: duration,
                           delay: 0,
                           usingSpringWithDamping: 1.0,
                           initialSpringVelocity: 0.0,
                           options: [.allowUserInteraction, .beginFromCurrentState],
                           animations: { self.transform = transform},
                           completion: nil)
        }
    }
}

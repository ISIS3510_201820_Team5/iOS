//
//  RecomendationCollectionViewCell.swift
//  Pina
//
//  Created by MilenioPC on 7/10/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class RecommendationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView! {
        didSet {
            productImage.layer.cornerRadius = 10
            productImage.clipsToBounds = true
        }
    }
    @IBOutlet var productName: UILabel!
    
    
    @IBOutlet weak var addProductToMyList: UIButton! {
        didSet {
            addProductToMyList.backgroundColor = .clear
            addProductToMyList.titleLabel?.textColor = UIColor(named: "Secundary 200")
            addProductToMyList.layer.cornerRadius = 10
            addProductToMyList.layer.borderWidth = 1
            addProductToMyList.layer.borderColor = UIColor(named: "Secundary 200")?.cgColor
            addProductToMyList.titleEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        }

    }
    
    func setCollectionCell(productName:String){
        self.productName.text = productName
    }
    
    override var isHighlighted: Bool {
        didSet {
            let duration = isHighlighted ? 0.45 : 0.4
            let transform = isHighlighted ?
                CGAffineTransform(scaleX: 0.96, y: 0.96) : CGAffineTransform.identity
            UIView.animate(withDuration: duration,
                           delay: 0,
                           usingSpringWithDamping: 1.0,
                           initialSpringVelocity: 0.0,
                           options: [.allowUserInteraction, .beginFromCurrentState],
                           animations: { self.transform = transform},
                           completion: nil)
        }
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 10
        layer.backgroundColor = UIColor(named: "Surface")?.cgColor
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 10)
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        layer.shadowOpacity = 0.5
        layer.shadowPath = shadowPath.cgPath

    }
}

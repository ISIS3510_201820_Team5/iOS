//
//  FeedNewListCell.swift
//  Pina
//
//  Created by Santiago Rojas on 10/27/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class FeedNewListCell: UICollectionViewCell {
    static let identifier = "FeedNewListCell"
    
    @IBOutlet weak var addListButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        clipsToBounds = true
        autoresizesSubviews = true
        
        addListButton.backgroundColor = UIColor(named: "Secundary 300")
        addListButton.layer.cornerRadius = addListButton.frame.size.width/2
                
        let shadowPath = UIBezierPath(roundedRect: addListButton.bounds, cornerRadius: addListButton.frame.size.width/2)
        addListButton.layer.masksToBounds = false
        addListButton.layer.shadowColor = UIColor.black.cgColor
        addListButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        addListButton.layer.shadowOpacity = 0.5
        addListButton.layer.shadowPath = shadowPath.cgPath
    }
    
    override var isHighlighted: Bool {
        didSet {
            let duration = isHighlighted ? 0.45 : 0.4
            let transform = isHighlighted ?
                CGAffineTransform(scaleX: 0.96, y: 0.96) : CGAffineTransform.identity
            UIView.animate(withDuration: duration,
                           delay: 0,
                           usingSpringWithDamping: 1.0,
                           initialSpringVelocity: 0.0,
                           options: [.allowUserInteraction, .beginFromCurrentState],
                           animations: { self.transform = transform},
                           completion: nil)
        }
    }

}

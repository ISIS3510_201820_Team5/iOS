//
//  ProductSearchTableViewCell.swift
//  Pina
//
//  Created by JUAN SEBASTIAN BARRAGAN JERONIMO on 10/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class PromotionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionPromotion: UILabel!
    
   
    @IBOutlet weak var marketName: UILabel!
    
    func configureCell(descriptions: String, market: String) {
       self.descriptionPromotion.text = descriptions
       self.marketName.text = market
    }
}

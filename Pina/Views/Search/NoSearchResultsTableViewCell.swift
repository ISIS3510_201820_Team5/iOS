//
//  NoSearchResultsTableViewCell.swift
//  Pina
//
//  Created by MilenioPC on 6/10/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class NoSearchResultsTableViewCell: UITableViewCell {

    @IBOutlet var NoFoundLabel: UILabel!
    
    func configureCell() {
        self.NoFoundLabel.text = "Ningún resultado.            Intenta buscado otro producto."
    }

}

//
//  PreviewView.swift
//  Pina
//
//  Created by Santiago Rojas on 11/6/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import AVKit

class PreviewView: UIView {
    
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
    
    /// Convenience wrapper to get layer as its statically known type.
    var videoPreviewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
    
}

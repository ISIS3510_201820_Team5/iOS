//
//  ListTableViewCell.swift
//  Pina
//
//  Created by SANTIAGO ROJAS HERRERA on 10/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var listTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

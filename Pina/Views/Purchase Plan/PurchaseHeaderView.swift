//
//  PurchasePlanHeaderView.swift
//  Pina
//
//  Created by Santiago Rojas on 11/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class PurchaseHeaderView: UICollectionReusableView {
    static let identifier = "PurchaseHeaderView"
    @IBOutlet weak var sectionLabel: UILabel!
    @IBOutlet weak var sectionPriceLabel: UILabel!
    
}

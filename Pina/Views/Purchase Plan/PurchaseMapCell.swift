//
//  PurchaseMapCell.swift
//  Pina
//
//  Created by Santiago Rojas on 11/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import MapKit

class PurchaseMapCell: UICollectionViewCell {
    static let identifier = "PurchaseMapCell"
    
    @IBOutlet weak var map: MKMapView!
}

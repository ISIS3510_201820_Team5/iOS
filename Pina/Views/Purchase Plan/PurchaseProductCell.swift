//
//  PurchaseProductCell.swift
//  Pina
//
//  Created by Santiago Rojas on 11/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class PurchaseProductCell: UICollectionViewCell {
    static let identifier = "PurchaseProductCell"
    
    @IBOutlet weak var productImage: UIImageView! {
        didSet {
            productImage.layer.cornerRadius = 10
            productImage.layer.borderWidth = 0.5
            productImage.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    @IBOutlet weak var productNameLabel: UILabel!
    
    @IBOutlet weak var productPriceLabel: UILabel!
}

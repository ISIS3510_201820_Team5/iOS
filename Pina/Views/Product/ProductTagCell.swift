//
//  ProductTagCell.swift
//  Pina
//
//  Created by Santiago Rojas on 10/21/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class ProductTagCell: UICollectionViewCell {
    static let identifier = "ProductTagCell"
    static let font = UIFont.preferredFont(forTextStyle: .caption1)
    
    let bgView = UIView()
    let nameLabel = UILabel()
    
    override func awakeFromNib() {
        
        clipsToBounds = true
        autoresizesSubviews = true
        
        layer.cornerRadius = 20
        layer.borderWidth = 1
        layer.borderColor = UIColor(named: "Secundary 50")?.cgColor
        
        bgView.frame = self.bounds
        bgView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.1019638271)
        backgroundView = bgView
        
        nameLabel.textColor = UIColor.darkGray
        nameLabel.font = ProductTagCell.font
        nameLabel.textAlignment = .center
        nameLabel.adjustsFontSizeToFitWidth = false
        nameLabel.allowsDefaultTighteningForTruncation = true
        nameLabel.adjustsFontForContentSizeCategory = true
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(nameLabel)
        
        setupConstraints()
    }
    
    func setupConstraints() {
        let constraints = [
            nameLabel.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            nameLabel.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    override var isHighlighted: Bool {
        didSet {
            let duration = isHighlighted ? 0.45 : 0.4
            let transform = isHighlighted ?
                CGAffineTransform(scaleX: 0.96, y: 0.96) : CGAffineTransform.identity
            let bgColor = isHighlighted ?
                UIColor(white: 1.0, alpha: 0.2) : UIColor(white: 1.0, alpha: 0.1)
            let animations = {
                self.transform = transform
                self.bgView.backgroundColor = bgColor
            }
            
            UIView.animate(withDuration: duration,
                           delay: 0,
                           usingSpringWithDamping: 1.0,
                           initialSpringVelocity: 0.0,
                           options: [.allowUserInteraction, .beginFromCurrentState],
                           animations: animations,
                           completion: nil)
        }
    }
    
    var tagName: String? {
        didSet {
            tagName = tagName?.uppercased()
            nameLabel.text = tagName
        }
    }
    
}

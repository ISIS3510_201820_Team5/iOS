//
//  ProductHistoricalPricesCell.swift
//  Pina
//
//  Created by Santiago Rojas on 10/21/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import ScrollableGraphView

class ProductHistoricalPricesCell: UICollectionViewCell {
    static let identifier = "ProductHistoricalPricesCell"
    
    @IBOutlet weak var graphView: ScrollableGraphView!
    
    @IBOutlet weak var recommendedLabel: UILabel!
    
    var shouldClientBuy: Bool? {
        willSet {
            guard let shouldBuy = newValue else { return }
            let linePlot = LinePlot(identifier: "line")
            linePlot.lineWidth = 2
            linePlot.lineStyle = .smooth
            linePlot.shouldFill = true
            linePlot.fillType = .solid
            linePlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
            
            let dotPlot = DotPlot(identifier: "lineDot") // Add dots as well.
            dotPlot.dataPointSize = 4
            dotPlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
            
            if shouldBuy {
                dotPlot.dataPointFillColor = UIColor(named: "Secundary 500")!
                linePlot.lineColor = UIColor(named: "Secundary 500")!
                linePlot.fillColor = UIColor(named: "Secundary 200")!.withAlphaComponent(0.5)
                recommendedLabel.text = NSLocalizedString("Should buy product", comment: "Should buy product")
                recommendedLabel.textColor = UIColor(named: "Secundary 500")!
                
            } else {
                dotPlot.dataPointFillColor = UIColor.red
                linePlot.lineColor = UIColor.red
                linePlot.fillColor = UIColor.red.withAlphaComponent(0.5)
                recommendedLabel.text = NSLocalizedString("Shouldn't buy product", comment: "Shouldn't buy product")
                recommendedLabel.textColor = UIColor(named: "Primary 500")!
            }
            
            graphView.addPlot(plot: linePlot)
            graphView.addPlot(plot: dotPlot)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let referenceLines = ReferenceLines()
        referenceLines.referenceLineLabelFont = UIFont.boldSystemFont(ofSize: 8)
        referenceLines.referenceLineColor = UIColor.black.withAlphaComponent(0.2)
        referenceLines.referenceLineLabelColor = UIColor.gray
        
        referenceLines.dataPointLabelColor = UIColor.black.withAlphaComponent(0.8)
        
        graphView.dataPointSpacing = 80
        graphView.shouldAdaptRange = true
        graphView.shouldAnimateOnAdapt = true
        graphView.shouldAnimateOnStartup = true
        graphView.direction = .rightToLeft
        
        
        graphView.addReferenceLines(referenceLines: referenceLines)
    }
}

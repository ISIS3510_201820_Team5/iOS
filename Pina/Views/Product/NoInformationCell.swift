//
//  NoInformationCell.swift
//  Pina
//
//  Created by Santiago Rojas on 10/21/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class NoInformationCell: UICollectionViewCell {
    static let identifier = "NoInformationCell"
    
    @IBOutlet weak var label: UILabel!
    
    var message: String? {
        didSet {
            if let message = message {
                label.text = NSLocalizedString(message, comment: message)
            }
        }
    }
}

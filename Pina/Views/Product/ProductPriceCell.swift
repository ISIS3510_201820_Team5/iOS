//
//  ProductPriceCell.swift
//  Pina
//
//  Created by Santiago Rojas on 10/21/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class ProductPriceCell: UICollectionViewCell {
    static let identifier = "ProductPriceCell"
    
    @IBOutlet weak var marketLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var productPrice: (name:String, price:Double)? {
        didSet {
            guard let productPrice = productPrice else { return }
            marketLabel.text = NSLocalizedString(productPrice.name, comment: "")
            priceLabel.text = "$ " + String(format: "%.0f", productPrice.price)
        }
    }
}

//
//  ProductSectionHeaderView.swift
//  Pina
//
//  Created by Santiago Rojas on 10/21/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class ProductSectionHeaderView: UICollectionReusableView {
    static let identifier = "ProductSectionHeader"
    @IBOutlet weak var sectionTitle: UILabel!
}

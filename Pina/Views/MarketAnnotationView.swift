//
//  MarketAnnotationView.swift
//  Pina
//
//  Created by Santiago Rojas on 12/9/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import MapKit

class MarketAnnotationView: MKMarkerAnnotationView {
    
//    override var annotation: MKAnnotation?
        
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

class MarketAnnotation: NSObject, MKAnnotation {
    
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    
    var markerTintColor: UIColor {
        guard let color = UIColor(named: title!) else { return .red }
        return color
    }

    
    init(title: String, subtitle: String?, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        
        super.init()
    }
    
}

class MarketMarkerView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let market = newValue as? MarketAnnotation else { return }
            canShowCallout = false
            calloutOffset = CGPoint(x: -5, y: 5)
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            markerTintColor = market.markerTintColor
            glyphText = String(market.title!.first!)
        }
    }
}

//
//  ListProductTableViewCell.swift
//  Pina
//
//  Created by SANTIAGO ROJAS HERRERA on 10/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import Firebase

class ListProductTableViewCell: UITableViewCell {
    
    var product : ListProduct?

    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    
    @IBAction func changeQuantity(_ sender: UIStepper, forEvent event: UIEvent) {
        product?.quantity = Int(stepper.value)
    }   
}

//
//  ListProductSearchTableViewCell.swift
//  Pina
//
//  Created by Santiago Rojas on 10/7/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class ListProductSearchTableViewCell: UITableViewCell {

    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var addProductButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

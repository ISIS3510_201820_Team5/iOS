//
//  PurchasePlanButton.swift
//  Pina
//
//  Created by Santiago Rojas on 11/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit

class PurchasePlanButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
                
        layer.cornerRadius = 10
        layer.backgroundColor = UIColor(named: "Secundary 200")?.cgColor
        
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 10)
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        layer.shadowOpacity = 0.5
        layer.shadowPath = shadowPath.cgPath
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
    }

    override var isHighlighted: Bool {
        didSet {
            let duration = isHighlighted ? 0.45 : 0.4
            let transform = isHighlighted ?
                CGAffineTransform(scaleX: 0.96, y: 0.96) : CGAffineTransform.identity
            UIView.animate(withDuration: duration,
                           delay: 0,
                           usingSpringWithDamping: 1.0,
                           initialSpringVelocity: 0.0,
                           options: [.allowUserInteraction, .beginFromCurrentState],
                           animations: { self.transform = transform},
                           completion: nil)
        }
    }

    
}

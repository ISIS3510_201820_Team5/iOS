//
//  DataService.swift
//  Pina
//
//  Created by JUAN SEBASTIAN BARRAGAN JERONIMO on 10/3/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

let DB_BASE = Database.database().reference()

class DataService {
    
    static let instance = DataService()
    
    lazy var functions = Functions.functions()
    
    private var _REF_BASE = DB_BASE
    
    private var _REF_PRODUCTS = DB_BASE.child("products")
    
    private var _REF_MARKETS = DB_BASE.child("markets")
    
    private var _REF_PRICES = DB_BASE.child("prices")
    
    private var _REF_TAGS = DB_BASE.child("tags")
    
    private var _REF_LISTS = DB_BASE.child("lists")
    
    private var _REF_USERS = DB_BASE.child("users")
    
    
    var REF_BASE: DatabaseReference {
        return _REF_BASE
    }
    
    var REF_PRODUCTS: DatabaseReference {
        return _REF_PRODUCTS
    }
    
    var REF_MARKETS: DatabaseReference {
        return _REF_MARKETS
    }
    
    var REF_PRICES: DatabaseReference {
        return _REF_PRICES
    }
    
    var REF_TAGS: DatabaseReference {
        return _REF_TAGS
    }
    
    var REF_LISTS: DatabaseReference {
        return _REF_LISTS
    }
    
    var REF_USERS: DatabaseReference {
        return _REF_USERS
    }
    
    //https://www.raywenderlich.com/3-firebase-tutorial-getting-started
    
    func getProductsStartingWith(searchValue: String, handler: @escaping (_ productListArray: [Product]) -> ()) {
        
        REF_PRODUCTS.queryOrdered(byChild: "name").queryStarting(atValue: searchValue).queryLimited(toFirst: 15).observeSingleEvent(of: .value) { (productsListSnapshot) in
            guard let productsListSnapshot = productsListSnapshot.children.allObjects as? [DataSnapshot] else { return }
            var productsListResults = [Product]()
            for product in productsListSnapshot {
                let barcode = Int(product.key)!
                let name = product.childSnapshot(forPath: "name").value as! String
                let tags = product.childSnapshot(forPath: "tags").value as! [String]
                let product = Product(key: product.key, barcode: barcode, name: name, tags: tags, prices: nil, historicalPrices: nil)
                productsListResults.append(product)
            }
            handler(productsListResults)
        }
        
    }
    
    func getProductPrices(productToInsertPrices: Product, handler: @escaping (_ product: Product) -> ()) {
        print("getting prices of ", productToInsertPrices)
        REF_PRICES.child(productToInsertPrices.key).observeSingleEvent(of: .value) { (pricesSnapshot) in
            guard let pricesSnapshot = pricesSnapshot.children.allObjects as? [DataSnapshot] else { return }
            print(pricesSnapshot)
            
            
            handler(productToInsertPrices)
        }
        
        
    }
    
    func loadLists(handler: @escaping (_ lists: [ListForUser]) -> ()) {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        
        DataService.instance.REF_USERS.child("\(userId)/lists/").queryOrdered(byChild: "order").observe(.value) { snapshot in
            var newItems = [ListForUser]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot {
                    let list = ListForUser(snapshot: snapshot)
                    newItems.append(list)
                }
            }
            handler(newItems)
        }
    }
    
    func getListsOwnedByUser() -> DatabaseReference? {
        guard let userId = Auth.auth().currentUser?.uid else { return nil }
        return REF_USERS.child(userId).child("lists")
    }
    
    func createList(withName name: String) -> ListForUser? {
        guard let userId = Auth.auth().currentUser?.uid else { return nil }
        
        let list = List(ref: REF_LISTS.childByAutoId())
        list.name = name
        list.ownerId = userId
        
        let listForUser = ListForUser(ref: REF_USERS.child("\(userId)/lists/\(list.key)"))
        listForUser.name = name
        listForUser.owner = true
        listForUser.order = 1 // TODO: Set correct order
        
        return listForUser
    }
    
    func deleteList(withKey key: String) {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        
        REF_LISTS.child(key).removeValue()
        REF_USERS.child("\(userId)/lists/\(key)").removeValue()
        
    }
    
    func getOwnerOfList(key: String) -> ListUser? {
        guard let userId = Auth.auth().currentUser?.uid else { return nil }
        return ListUser(name: "User", id: userId)
    }
    
    func addProduct(_ product: Product, toListWithKey listKey: String) {
        let productRef = REF_LISTS.child("\(listKey)/products/\(product.key)")
        productRef.updateChildValues(["name": product.name, "barcode": product.barcode, "quantity": 1])
    }
    
    /*
    //MARK: Purchase Plan with onCall Cloud Function
    func getPurchasePlan(forListWithKey listKey: String) {

        functions.httpsCallable("purchasePlan").call(["listId": listKey]) { (result, error) in
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            if let purchasePlanData = (result?.data as? [String: Any]) {
                print(purchasePlanData)
            }
        }

    }
    */

    //MARK: Purchase Plan with onRequest Cloud Function
    func getPurchasePlan(forListWithKey listKey: String, handler: @escaping (_ purchasePlan: PurchasePlan) -> Void) {
        
        let url = URL(string: "https://us-central1-pina-9922f.cloudfunctions.net/createPurchasePlan")!
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let json = ["listId": listKey]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                let purchasePlan = PurchasePlan(date: Date(), listKey: listKey, data: responseJSON)
                handler(purchasePlan)
            }
        }
        task.resume()

    }
    
    // Mark: Scanner
    func isProductOnDatabase(barcode: String, handler: @escaping (_ result: Bool) -> Void) {
        REF_PRODUCTS.child(barcode).observeSingleEvent(of: .value) { (snapshot) in
            handler(snapshot.exists())
        }

    }
    
    func getMarketLocations(handler: @escaping (_ markets: [String: Market]) -> Void) {
        REF_MARKETS.observeSingleEvent(of: .value) { (snapshot) in
            var markets = [String: Market]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let market = Market(snapshot: snapshot) {
                    markets[snapshot.key] = market
                }
            }
            handler(markets)
        }
    }
}

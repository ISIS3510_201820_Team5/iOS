//
//  Product.swift
//  Pina
//
//  Created by JUAN SEBASTIAN BARRAGAN JERONIMO on 10/3/18.
//  Copyright © 2018 Piña. All rights reserved.
//

//
//  Product.swift
//  Pina
//
//  Created by JUAN SEBASTIAN BARRAGAN JERONIMO on 10/3/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import Foundation
import Firebase
import Surge

struct Product {
    
    init(key: String, barcode: Int, name: String, tags: [String], prices: [(name:String, price:Double)]?, historicalPrices: [(date:Date, price:Double)]?) {
        self.key = key
        self.barcode = barcode
        self.name = name
        self.tags = tags
        self.prices = prices
        self.historicalPrices = historicalPrices ?? [(date:Date, price:Double)]()
    }
    
    init?(snapshot: DataSnapshot) {
        guard let values = snapshot.value as? [String: AnyObject] else { return nil }
        self.key = snapshot.key
        self.barcode = Int(snapshot.key) ?? 0
        self.name = values["name"] as? String ?? ""
        self.tags = values["tags"] as? [String] ?? [String]()
    }
    
    let key: String
    let barcode: Int
    let name: String
    let tags: [String]
    
    var prices: [(name:String, price:Double)]?
    var historicalPrices = [(date:Date, price:Double)]()
    
    mutating func updatePrices(fromSnapshot snapshot: DataSnapshot) {
        guard let values = snapshot.value as? [String: AnyObject] else { return }
        let orderedValues = values.sorted { $0.key < $1.key }
        if let currentPrices = orderedValues.last?.value as? [String: Double] {
            self.prices = currentPrices.map { (name: $0.key, price: $0.value) }
            self.prices?.sort { $0.price < $1.price }
        }
        for value in orderedValues {
            guard let prices = value.value as? [String: Double] else { continue }
            let date = Date(timeIntervalSince1970: Double(value.key)!/1000)
            let averagePrice = prices["average"]
            self.historicalPrices.append((date: date, price: averagePrice!))
        }
    }
    
    func shouldClientBuy() -> Bool {
        let prices = historicalPrices.map { $0.price }
        let mean = Surge.mean(prices)
        if let currentAverage = (self.prices?.first {$0.name == "average"})?.price {
            return currentAverage < mean
        } else {
            return false
        }
    }
}

//
//  List.swift
//  Pina
//
//  Created by JUAN SEBASTIAN BARRAGAN JERONIMO on 10/3/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import Foundation
import Firebase

class List {
    
    init(ref: DatabaseReference) {
        self.ref = ref
        self.ref.keepSynced(true)
        
        self.ref.observe(.value) { snapshot in
            if let values = snapshot.value as? [String: AnyObject] {
                self._ownerId = values["user_id"] as? String
                self._name = values["name"] as? String
            }
            
        }
    }
    
    convenience init(snapshot: DataSnapshot) {
        //        self.ref = snapshot.ref
        //        if let values = snapshot.value as? [String: AnyObject] {
        //            self._ownerId = values["owner_Id"] as? String
        //            self._name = values["name"] as? String
        //        }
        self.init(ref: snapshot.ref)
    }
    
    let ref: DatabaseReference
    
    var key: String { return ref.key! }
    
    private var _ownerId: String?
    var ownerId: String? {
        get { return _ownerId }
        set {
            ref.child("user_id").setValue(newValue)
            _ownerId = newValue
        }
    }
    
    private var _name: String?
    var name: String? {
        get { return _name }
        set {
            ref.child("name").setValue(newValue)
            _name = newValue
        }
    }
    
    var products: [ListProduct] { return [ListProduct]() }
    
    var participants: [ListUser] { return [ListUser]() }
    
    func toAnyObject() -> Any {
        return [
            "name": name,
            "owner_id": ownerId,
        ]
    }
}


class ListForUser {
    init(ref: DatabaseReference) {
        self.ref = ref
        self.ref.keepSynced(true)
    }
    
    convenience init(snapshot: DataSnapshot) {
        self.init(ref: snapshot.ref)
        if let values = snapshot.value as? [String: AnyObject] {
            self._name = values["name"] as? String
            self._order = values["order"] as? Int
            self._owner = values["owner"] as? Bool
        }
    }
    
    let ref: DatabaseReference
    
    var key: String { return ref.key! }
    
    private var _name: String?
    var name: String? {
        get { return _name }
        set {
            ref.child("name").setValue(newValue)
            _name = newValue
        }
    }
    
    private var _order: Int?
    var order: Int? {
        get { return _order }
        set {
            ref.child("order").setValue(newValue)
            _order = newValue
        }
    }
    
    private var _owner: Bool?
    var owner: Bool! {
        get { return _owner }
        set {
            ref.child("owner").setValue(newValue)
            _owner = newValue
        }
    }
    
    //    func toAnyObject() -> Any {
    //        return [
    //            "name": name,
    //            "order": order,
    //            "owner": owner
    //        ]
    //    }
    //
    //
}


class ListProduct {
    
    init?(snapshot: DataSnapshot) {
        guard let values = snapshot.value as? [String: AnyObject] else { return nil }
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.name = values["name"] as? String
        self._quantity = values["quantity"] as? Int ?? 1
    }
    
    let ref: DatabaseReference
    
    let key: String
    
    let name: String?
    
    private var _quantity: Int
    var quantity: Int {
        get { return _quantity }
        set { ref.child("quantity").setValue(newValue)
            _quantity = newValue
        }
    }
}

struct ListUser {
    
    let name: String
    
    let id: String
    
}

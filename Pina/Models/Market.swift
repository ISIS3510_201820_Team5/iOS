//
//  Market.swift
//  Pina
//
//  Created by JUAN SEBASTIAN BARRAGAN JERONIMO on 10/3/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import Foundation
import Firebase

struct Market {
    let name:String
    let locations:[Coordinate]
    //    let logo:String
    
    init?(snapshot: DataSnapshot) {
        guard let values = snapshot.value as? [String] else {return nil}
        
        var locations = [Coordinate]()
        for value in values {
            let coordinates = value.split(separator: ",")
            if let latitude = Double(coordinates[0]), let longitude = Double(coordinates[1]) {
                locations.append(Coordinate(latitude: latitude, longitude: longitude))
            }
        }
        self.name = snapshot.key
        self.locations = locations
    }
    
}

struct Coordinate {
    let latitude:Double
    let longitude:Double
}

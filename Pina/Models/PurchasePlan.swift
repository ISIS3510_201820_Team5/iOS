//
//  PurchasePlan.swift
//  Pina
//
//  Created by Santiago Rojas on 11/5/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import Foundation

struct PurchasePlan {
    
    init(date: Date, listKey: String, data: [String: Any]) {
        var prices = data
        
        self.date = date
        self.listKey = listKey
        self.totalPrice = prices.removeValue(forKey: "total") as! Double
        
        self.markets = [String]()
        self.marketTotal = [Double]()
        self.marketProducts = [[PurchasePlanProduct]]()
        
        var index = 0
        for (market, data) in prices {
            self.markets.append(market)
            let marketTotal = (data as! [String: Any])["total"] as? Double ?? 0
            self.marketTotal.append(marketTotal)
            let products = (data as! [String: Any])["products"] as! [[String: Any]]
            self.marketProducts.append([PurchasePlanProduct]())
            for product in products {
                let key = product["barcode"] as! String
                let name = product["name"] as! String
                let quantity = product["quantity"] as! Int
                let price = product["price"] as! Double
                
                let purchasePlanProduct = PurchasePlanProduct(key: key, name: name, quantity: quantity, price: price)
                self.marketProducts[index].append(purchasePlanProduct)
            }
            
            index += 1
        }
        
    }
    
    let listKey: String
    let date: Date
    
    let totalPrice: Double
    var markets: [String]
    var marketTotal: [Double]
    var marketProducts: [[PurchasePlanProduct]]
}

struct PurchasePlanProduct {
    let key: String
    let name: String
    let quantity: Int
    let price: Double
}

//
//  ScannerViewController.swift
//  Pina
//
//  Created by Santiago Rojas on 11/5/18.
//  Copyright © 2018 Piña. All rights reserved.
//
//  Based on: CDBarcodes - BarcodeReaderViewController.swift
//  Created by Matthew Maher on 1/29/16.
//

import UIKit
import AVFoundation
import Firebase

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var captureSession: AVCaptureSession!
    var productKey: String!
    
    @IBOutlet var previewView: PreviewView!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.setupCaptureSession()
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    self.setupCaptureSession()
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        case .denied:
            fallthrough
        case .restricted:
            fallthrough
        default:
            navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
//        previewView.frame = view.layer.frame
        previewLayer.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: size)

//        if let connection =  self.previewView.videoPreviewLayer.connection {
        if let connection =  previewLayer.connection {
            let currentDevice = UIDevice.current
            let orientation = currentDevice.orientation
            
            if (connection.isVideoOrientationSupported) {
                switch (orientation) {
                case .portrait:
                    connection.videoOrientation = .portrait
                case .landscapeRight:
                    connection.videoOrientation = .landscapeLeft
                case .landscapeLeft:
                    connection.videoOrientation = .landscapeRight
                case .portraitUpsideDown:
                    connection.videoOrientation = .portraitUpsideDown
                default:
                    connection.videoOrientation = .portrait
                }
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (captureSession?.isRunning == false) {
                captureSession.startRunning()
        }
        
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = UIColor.clear
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        
        tabBarController?.tabBar.tintColor = UIColor(named: "Secundary 300")

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.largeTitleDisplayMode = .automatic
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func setupCaptureSession() {
        
        captureSession = AVCaptureSession()
        captureSession.beginConfiguration()
        
        let videoDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .unspecified)

        guard let videoDeviceInput = try? AVCaptureDeviceInput(device: videoDevice!), captureSession.canAddInput(videoDeviceInput)
            else { return }
        
        captureSession.addInput(videoDeviceInput)
        let metadataOutput = AVCaptureMetadataOutput()
        
        guard captureSession.canAddOutput(metadataOutput) else { return }
        captureSession.addOutput(metadataOutput)
            
        metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metadataOutput.metadataObjectTypes = [.ean13, .ean8, .upce]
        
//        previewView.videoPreviewLayer.session = captureSession
//        previewView.videoPreviewLayer.videoGravity = .resizeAspectFill;
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer);
        
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier ?? "" {
        case "Show Product":
            if let productViewController = segue.destination as? ProductViewController {
                productViewController.productKey = productKey
            }
        default:
            fatalError("Unidentified Segue")
        }

     }
     
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if let barcodeData = metadataObjects.first as? AVMetadataMachineReadableCodeObject {
            
            barcodeDetected(code: barcodeData.stringValue ?? "0");
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
    }
    
    func barcodeDetected(code: String) {
        
        var trimmedCode = code.trimmingCharacters(in: .whitespacesAndNewlines)
        
        while trimmedCode.hasPrefix("0") && trimmedCode.count > 1 {
            trimmedCode = String(trimmedCode.dropFirst())
        }
        
        captureSession.stopRunning()
        
        DataService.instance.isProductOnDatabase(barcode: trimmedCode) { (isOnDatabase) in
            if isOnDatabase {
                self.productKey = trimmedCode
                self.performSegue(withIdentifier: "Show Product", sender: self)
            } else {
                let alert = UIAlertController(title: NSLocalizedString("Not on Database", comment: "Product not on database"), message: code, preferredStyle: .alert)
                self.present(alert, animated: true)
                
                self.captureSession.startRunning()
                let duration: Double = 0.75
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                    alert.dismiss(animated: true)
                }
                
            }
        }
        
    }


}


//
//  PurchasePlanViewController.swift
//  Pina
//
//  Created by Santiago Rojas on 11/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import MapKit
import Foundation
import Firebase
import FirebaseUI

class PurchasePlanViewController: UICollectionViewController {

    let activityView = UIActivityIndicatorView(style: .gray)
    let fadeView = UIView()
    var locationManager = CLLocationManager()
    
    var isPlanLoaded = false
    var products: [ListProduct]!
    var list: List!
    
    var purchasePlan: PurchasePlan?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = NSLocalizedString("Purchase Plan", comment: "Purchase Plan")

        collectionView.dataSource = self
        collectionView.delegate = self
        
        DataService.instance.getPurchasePlan(forListWithKey: list.key) { (purchasePlan) in
            self.purchasePlan = purchasePlan
            self.isPlanLoaded = true
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                    self.collectionView.reloadData()
                    self.fadeView.alpha = 0
                    self.activityView.stopAnimating()
                    self.fadeView.removeFromSuperview()
                }, completion: nil)
            }
            
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.barTintColor = UIColor(named: "Surface")
        navigationController?.navigationBar.tintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
        tabBarController?.tabBar.tintColor = UIColor(named: "Secundary 300")
        
        if !isPlanLoaded {
            fadeView.frame = view.frame
            fadeView.backgroundColor = UIColor(named: "Surface")
            fadeView.alpha = 1
            
            view.addSubview(fadeView)
            view.addSubview(activityView)
            
            activityView.hidesWhenStopped = true
            activityView.center = view.center
            activityView.startAnimating()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier ?? "" {
        case "Show Product Detail":
            if let cell = sender as? PurchaseProductCell, let productViewController = segue.destination as? ProductViewController {
                let index = collectionView.indexPath(for: cell)!
                let productKey = purchasePlan!.marketProducts[index.section-1][index.item].key
                productViewController.productKey = productKey
            }
        default:
            fatalError("Unidentified Segue")
        }
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (purchasePlan?.markets.count ?? 0) + 1
    }

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard kind == UICollectionView.elementKindSectionHeader else { assert(false, "Unexpected element kind") }
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: PurchaseHeaderView.identifier, for: indexPath) as! PurchaseHeaderView
        switch indexPath.section {
        case 0:
            if let purchasePlan = self.purchasePlan {
                headerView.sectionLabel.text = DateFormatter.localizedString(from: purchasePlan.date, dateStyle: .medium, timeStyle: .none)
                headerView.sectionPriceLabel.text = "$ " + String(format: "%.0f", purchasePlan.totalPrice)
            }
        case let index:
            if let purchasePlan = self.purchasePlan {
                headerView.sectionLabel.text = NSLocalizedString("Buy in", comment: "Buy in") + NSLocalizedString(purchasePlan.markets[index-1], comment: "Market Name")
                headerView.sectionPriceLabel.text = "$ " + String(format: "%.0f", purchasePlan.marketTotal[index-1])
            }
            
        }
        return headerView
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        switch section {
        case 0:
            return 1
        default:
            guard let purchasePlan = self.purchasePlan else { return 0 }
            print("Number of items in section: \(purchasePlan.marketProducts[section-1].count)")
            return purchasePlan.marketProducts[section-1].count
        
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PurchaseMapCell.identifier, for: indexPath) as? PurchaseMapCell
                else { preconditionFailure("Failed to load collection view cell") }
            
            cell.map.register(MarketMarkerView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
            cell.map.mapType = .mutedStandard
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
            }
            
            if let userLocation = locationManager.location?.coordinate {
                let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 2000, longitudinalMeters: 2000)
                cell.map.setRegion(viewRegion, animated: true)
            }
            
            DataService.instance.getMarketLocations { (markets) in
                for (marketName, market) in markets {
                    for location in market.locations {
                        let annotation = MarketAnnotation(title: NSLocalizedString(marketName, comment: marketName), subtitle: nil, coordinate: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
                        cell.map.addAnnotation(annotation)
                    }
                    
                }
            }
            
            return cell
        default:
            guard let product = self.purchasePlan?.marketProducts[indexPath.section - 1][indexPath.item] else { break }
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PurchaseProductCell.identifier, for: indexPath) as? PurchaseProductCell else { preconditionFailure("Failed to load collection view cell") }
            
            cell.productNameLabel.text = product.name
            cell.productPriceLabel.text = "$ " + String(format: "%.0f", product.price * Double(product.quantity))
            let imageRef = Storage.storage().reference(withPath: "products/\(product.key)/thumbnail.jpg")
            cell.productImage.sd_setImage(with: imageRef, placeholderImage: UIImage(named: "Product Placeholder"))
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NoInformationCell.identifier, for: indexPath) as? NoInformationCell else { preconditionFailure("Failed to load collection view cell") }
        cell.message = "There is no information available"
        return cell
        
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}

extension PurchasePlanViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width , height: view.frame.height/3)
        default:
            return CGSize(width: view.frame.width, height: 80)
        }
    }
}

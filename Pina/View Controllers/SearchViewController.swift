//
//  SearchViewController.swift
//  Pina
//
//  Created by SANTIAGO ROJAS HERRERA on 9/27/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit


class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var navigationBar: UINavigationItem!
    
    @IBOutlet weak var tableView: UITableView!
    
    var products = [Product]()
    
    var promotions = [Promotion]()
    
    var lastTimeQuerySended = Date().timeIntervalSince1970
    
    var haveNotPressedSearchButtom = true
    
    let searchController = UISearchController(searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        promotions.append(Promotion(descriptions:"Compra una bolsa de leche y te regalamos 30 ! ", marketName:"Carulla"))
        
        promotions.append(Promotion(descriptions:"Aniversario 30 años con 30% de descuento en toda la tienda ", marketName:"Éxito"))
        promotions.append(Promotion(descriptions:"Carnes Rojas con el 5 %  de descuento ", marketName:"Olímpica"))
        
        promotions.append(Promotion(descriptions:"Por cada 200.000 pesos en compras llevas cubeta de 30 huevos gratis", marketName:"Jumbo"))
        
        //        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .topAttached, barMetrics: .default)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        
        tableView.tableFooterView = UIView()
        navigationBar.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.tintColor = UIColor(named: "Primary 300")
        definesPresentationContext = true
        navigationItem.searchController = searchController
        
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = UIColor(named: "Primary 300")
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor(named: "Primary 300")!]
        tabBarController?.tabBar.tintColor = UIColor(named: "Primary 300")
        
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(searchController.isActive && !self.haveNotPressedSearchButtom && self.products.count==0) {
            tableView.separatorColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
            return 1
        }else if (searchController.isActive){
            tableView.separatorColor = UIColor.lightGray
            return products.count
        }
        else {
            tableView.separatorColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
            return promotions.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var typeCell:String
        
        if (searchController.isActive && !self.haveNotPressedSearchButtom && self.products.count==0) {
            typeCell = "NoResults"
            
        }
        else if (searchController.isActive) {
            typeCell = "Cell"
            
        }
        else {
            typeCell = "Promotion"
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: typeCell, for:indexPath)
        
        if let promotionCell = cell as? PromotionTableViewCell{
            promotionCell.configureCell(descriptions: promotions[indexPath.row].descriptions, market:promotions[indexPath.row].marketName)
            return promotionCell
        }
        else{
            
            if let noResultsCell = cell as? NoSearchResultsTableViewCell{
                noResultsCell.configureCell()
                self.haveNotPressedSearchButtom = true
                return noResultsCell
            }
            else {
                cell.textLabel!.text = products[indexPath.row].name
                cell.detailTextLabel!.text = products[indexPath.row].tags.joined(separator: ", ")
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
      
        
        if (searchController.isActive && self.haveNotPressedSearchButtom && self.products.count==0) {
            //typeCell = "NoResults"
            
        }
        else if (searchController.isActive) {
            //typeCell = "Cell"
            let productKey = self.products[indexPath.row].key
            performSegue(withIdentifier: "Show Product Detail", sender: productKey)
        }
        else {
           // typeCell = "Promotion"
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return searchController.isActive
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let productViewController = segue.destination as? ProductViewController {
            productViewController.productKey = sender as? String
        }
    }
}


extension SearchViewController: UISearchResultsUpdating, UISearchBarDelegate{
    
    func updateSearchResults(for searchController: UISearchController) {
        if (Date().timeIntervalSince1970 - self.lastTimeQuerySended > 1.5){
            if let text = searchController.searchBar.text,
                !text.isEmpty , text.count<90 {
                
                DataService.instance.getProductsStartingWith(searchValue: text, handler:  { (products) in
      
                    self.lastTimeQuerySended = Date().timeIntervalSince1970
                    self.products = products
                    self.tableView.reloadData()
                })
            }
            else {
                products = [Product]()
                tableView.reloadData()
            }
            
        }
        else {
            tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.haveNotPressedSearchButtom = false
        tableView.separatorColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        tableView.reloadData()
    }
}


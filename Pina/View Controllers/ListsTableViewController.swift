//
//  ListsTableViewController.swift
//  Pina
//
//  Created by SANTIAGO ROJAS HERRERA on 10/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import Firebase

class ListsTableViewController: UITableViewController {
    
    var lists = [ListForUser]()
    var newList: ListForUser?
    
    var openCreateListDialog = false
    var alertController = UIAlertController()
    
    
    private func loadLists() {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        
        DataService.instance.REF_USERS.child("\(userId)/lists/").queryOrdered(byChild: "order").observe(.value) { snapshot in
            var newItems = [ListForUser]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot {
                    let list = ListForUser(snapshot: snapshot)
                    newItems.append(list)
                }
            }
            
            self.lists = newItems
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: Actions
    
    @IBAction func createList(_ sender: Any?) {
        
        alertController = createNewListPrompt()
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let text = textField.text, text.count > 24 {
            textField.text = String(text.dropLast())
        }
        
        let text = textField.text ?? ""
        alertController.actions[0].isEnabled = !text.isEmpty
    }
    
    func createNewListPrompt() -> UIAlertController {
        
        let alertController = UIAlertController(title: NSLocalizedString("New List", comment: ""), message: NSLocalizedString("New List message", comment: ""), preferredStyle: .alert)
        
        alertController.view.tintColor = UIColor(named: "Orange")
        
        var listNameTextField: UITextField!
        let save = UIAlertAction(title: NSLocalizedString("Save", comment: ""), style: .default) { (action) -> Void in
            if let listName = listNameTextField?.text {
                self.newList = DataService.instance.createList(withName: listName)
                
                //let newIndexPath = IndexPath(row: self.lists.count, section: 0)
                
                self.lists.append(self.newList!)
                //self.tableView.insertRows(at: [newIndexPath], with: .automatic)
                self.performSegue(withIdentifier: "New List", sender: nil)
            }
            
            
        }
        save.isEnabled = false
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel)
        alertController.addAction(save)
        alertController.addAction(cancel)
        alertController.addTextField { (textField) -> Void in
            // Enter the textfiled customization code here.
            listNameTextField = textField
            listNameTextField?.addTarget(self, action:  #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            listNameTextField?.placeholder = NSLocalizedString("New List placeholder", comment: "")
        }
        
        return alertController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    
        if openCreateListDialog {
            createList(self)
            openCreateListDialog = false
        }
        
        loadLists()
        
        // Set navbar to white when on top
        //        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = UIColor(named: "Orange")
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]

        tabBarController?.tabBar.tintColor = UIColor(named: "Orange")
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return lists.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ListTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ListTableViewCell else {
            fatalError("The dequeued cell is not an instance of ListTableViewCell.")
        }
        
        let list = lists[indexPath.row]
        
        cell.listTitleLabel.text = list.name
        
        return cell
    }
    
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let listToDelete = lists[indexPath.row]
            DataService.instance.deleteList(withKey: listToDelete.key)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.

        switch segue.identifier ?? "" {
        case "New List":
            if let listViewController = segue.destination as? ListViewController, let list = newList {
                listViewController.listForUser = list
            }
        case "Show Detail":
            if let cell = sender as? ListTableViewCell, let listViewController = segue.destination as? ListViewController {
                let indexPath = tableView.indexPath(for: cell)
                listViewController.listForUser = lists[indexPath?.row ?? 0]
            }
        default:
            fatalError("Unidentified Segue: ")
        }
        
    }
    
}

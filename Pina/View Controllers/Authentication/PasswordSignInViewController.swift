//
//  PasswordSignInViewController.swift
//  Pina
//
//  Created by Santiago Rojas on 10/26/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import FirebaseUI

class PasswordSignInViewController: FUIPasswordSignInViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var connectionLabel: UILabel!
    
    let reachability = Reachability()!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, authUI: FUIAuth, email: String?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil, authUI: authUI, email: email)
        
        emailTextField.text = email
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.rightBarButtonItem = nil
        navigationItem.title = ""
        
        updateTextFieldValue(nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        logInButton.layer.cornerRadius = 5
        logInButton.tintColor = UIColor.white
        logInButton.backgroundColor = UIColor(named: "Secundary 200")
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        reachability.whenReachable = { _ in
            self.logInButton.isEnabled = true
            self.forgotPasswordButton.isEnabled = true
            self.connectionLabel.isHidden = true
        }
        reachability.whenUnreachable = { _ in
            self.logInButton.isEnabled = false
            self.forgotPasswordButton.isEnabled = false
            self.connectionLabel.isHidden = false
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }


    }
    
    override func viewWillDisappear(_ animated: Bool) {
        reachability.stopNotifier()
    }
    
    @IBAction func updateTextFieldValue(_ sender: AnyObject?) {
        if let email = emailTextField.text, let password = passwordTextField.text, reachability.connection != .none {
            logInButton.isEnabled = !email.isEmpty && !password.isEmpty
            didChangeEmail(email, andPassword: password)
        }
    }
    
    @IBAction func onLogIn(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            signIn(withDefaultValue: email, andPassword: password)
        }
    }
    
    @IBAction func onForgotPassword(_ sender: UIButton) {
        if let email = emailTextField.text {
            forgotPassword(forEmail: email)
        }

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField, let email = emailTextField.text, let password = passwordTextField.text, reachability.connection != .none {
            signIn(withDefaultValue: email, andPassword: password)
        } else {
            textField.resignFirstResponder()
        }
        return false
    }

    @IBAction func onViewSelected(_ sender: Any) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
}

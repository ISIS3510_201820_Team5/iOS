//
//  EmailEntryViewController.swift
//  Pina
//
//  Created by Santiago Rojas on 10/26/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import FirebaseUI

class EmailEntryViewController: FUIEmailEntryViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var connectionLabel: UILabel!
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButton.layer.cornerRadius = 5
        nextButton.tintColor = UIColor.white
        nextButton.backgroundColor = UIColor(named: "Secundary 200")
        
        emailTextField.delegate = self
        
        reachability.whenReachable = { _ in
            self.nextButton.isEnabled = true
            self.connectionLabel.isHidden = true
        }
        reachability.whenUnreachable = { _ in
            self.nextButton.isEnabled = false
            self.connectionLabel.isHidden = false
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.tintColor = UIColor(named: "Secundary 400")
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        
        navigationItem.rightBarButtonItem = nil
        navigationItem.title = ""
        updateEmailValue(emailTextField)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        reachability.stopNotifier()
    }
    
    @IBAction func onViewSelected(_ sender: Any) {
        emailTextField.resignFirstResponder()
    }
    @IBAction func updateEmailValue(_ sender: UITextField) {
        if emailTextField == sender, let email = emailTextField.text, reachability.connection != .none {
            nextButton.isEnabled = !email.isEmpty
            didChangeEmail(email)
        }
    }
    
    @IBAction func onNextButton(_ sender: Any) {
        guard let email = emailTextField.text else { return }
        onNext(email)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField, let email = textField.text, reachability.connection != .none {
            onNext(email)
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

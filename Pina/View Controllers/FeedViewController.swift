//
//  FeedViewController.swift
//  Pina
//
//  Created by Santiago Rojas on 9/26/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class FeedViewController: UIViewController {
    
    var locationManager = CLLocationManager()
    
    var products = [Product]()
    var markets = [String: Market]()
    var lists = [ListForUser]()
    
    @IBOutlet weak var recommendationCollection: UICollectionView!
    @IBOutlet weak var listsCollection: UICollectionView!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        mapView.delegate = self
        mapView.mapType = .mutedStandard
        mapView.showsUserLocation = true
        mapView.register(MarketMarkerView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        listsCollection.dataSource = self
        listsCollection.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
        }
        
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 4000, longitudinalMeters: 4000)
            mapView.setRegion(viewRegion, animated: false)
        }
        
        DataService.instance.getMarketLocations { (markets) in
            self.markets = markets
            for (marketName, market) in markets {
                for location in market.locations {
                    let annotation = MarketAnnotation(title: NSLocalizedString(marketName, comment: marketName), subtitle: nil, coordinate: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
                    self.mapView.addAnnotation(annotation)
                }
                
            }
        }
        
        DataService.instance.getProductsStartingWith(searchValue: "A", handler:  { (products) in
            self.products = products
            self.recommendationCollection.dataSource = self
            self.recommendationCollection.delegate = self
        })
        
        loadLists()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = UIColor(named: "Secundary 300")
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor(named: "Secundary 300")!]
        tabBarController?.tabBar.tintColor = UIColor(named: "Secundary 300")
        
    }
    
    private func loadLists() {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        
        DataService.instance.REF_USERS.child("\(userId)/lists/").queryOrdered(byChild: "order").queryLimited(toFirst: 5).observe(.value) { snapshot in
            var newItems = [ListForUser]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot {
                    let list = ListForUser(snapshot: snapshot)
                    newItems.append(list)
                }
            }
            self.lists = newItems
            self.listsCollection.reloadData()
        }
    }
    
    
    @IBAction func signOut(_ sender: UIBarButtonItem) {
        try! Auth.auth().signOut()
    }
    
    
    @IBAction func addProductToList(_ sender: UIButton) {
        
        let product = products[sender.tag]
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel) { (action) in
            self.dismiss(animated: true)
        }

        let alert = UIAlertController(title: product.name , message: NSLocalizedString("Add the product to a List", comment: "Add the product to a List") , preferredStyle: .actionSheet)
        alert.addAction(cancelAction)
        
        alert.view.tintColor = UIColor(named: "Secundary 300")
        
        guard let tabBarLists = (tabBarController as? TabBarViewController)?.lists, tabBarLists.count > 0 else {
            
            let alert = UIAlertController(title: nil, message: NSLocalizedString("There are no lists", comment: ""), preferredStyle: .alert)
            self.present(alert, animated: true)
            
            // duration in seconds
            let duration = 0.75
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
                
            }
            return
        }
        
        for list in tabBarLists {
            let action = UIAlertAction(title: list.name, style: .default) { (_) -> Void in
                DataService.instance.addProduct(product, toListWithKey: list.key)
                
                let addedAlert = UIAlertController(title: nil, message: NSLocalizedString("Product added to your list", comment: ""), preferredStyle: .alert)
                self.present(addedAlert, animated: true)
                
                let duration = 0.75
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                    addedAlert.dismiss(animated: true)
                    
                }
            }
            alert.addAction(action)
            
        }
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier ?? "" {
        case "Show Product":
            if let cell = sender as? RecommendationCollectionViewCell, let productViewController = segue.destination as? ProductViewController {
                let index = recommendationCollection.indexPath(for: cell)?.item ?? 0
                productViewController.productKey = products[index].key
            }
        case "Show List":
            if let cell = sender as? FeedListCell, let listViewController = segue.destination as? ListViewController {
                let index = listsCollection.indexPath(for: cell)?.item ?? 0
                listViewController.listForUser = lists[index-1]
            }
        case "Create New List":
            if let listsViewController = segue.destination as? ListsTableViewController {
                listsViewController.openCreateListDialog = true
            }
        default:
            fatalError("Unidentified Segue")
        }
        
    }
}

extension FeedViewController: MKMapViewDelegate {

//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        guard let annotation = annotation as? MarketAnnotation else { return nil }
//        let identifier = "marker"
//        var view: MKMarkerAnnotationView
//        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
//            as? MKMarkerAnnotationView {
//            dequeuedView.annotation = annotation
//            view = dequeuedView
//        } else {
//            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//            view.canShowCallout = false
//            view.calloutOffset = CGPoint(x: -5, y: 5)
//            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
//        }
//        return view
//    }
//    
}

extension FeedViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let userLocation = locations.last?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 4000, longitudinalMeters: 4000)
            mapView.setRegion(viewRegion, animated: false)
        }
    }
}

extension FeedViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case recommendationCollection:
            return products.count
        case listsCollection:
            return lists.count + 1
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell
        switch collectionView {
        case recommendationCollection:
            cell = recommendationCollection.dequeueReusableCell(withReuseIdentifier: "Recommendation", for:indexPath)
            
            if let recommendation  = cell as? RecommendationCollectionViewCell {
                recommendation.setCollectionCell(productName: products[indexPath.row].name)
                
                let imageRef = Storage.storage().reference(withPath: "products/\(products[indexPath.row].key)/image.jpg")
                recommendation.productImage.sd_setImage(with: imageRef, placeholderImage: UIImage(named: "Product Placeholder"))
                recommendation.addProductToMyList.tag = indexPath.row
                
            }
        case listsCollection:
            if indexPath.item == 0 {
                cell = listsCollection.dequeueReusableCell(withReuseIdentifier: FeedNewListCell.identifier, for: indexPath)
            }
            else {
                cell = listsCollection.dequeueReusableCell(withReuseIdentifier: FeedListCell.identifier, for: indexPath)
                (cell as? FeedListCell)?.listNameLabel.text = lists[indexPath.item - 1].name
            }
        default:
            cell = UICollectionViewCell()
        }
        return cell
    }

}

extension FeedViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch (collectionView, indexPath.item) {
        case (recommendationCollection, _):
            return CGSize(width: 166, height: 216)
        case (listsCollection, 0):
            return CGSize(width: 109, height: 109)
        case (listsCollection, _):
            return CGSize(width: 148, height: 109)
        default:
            break
        }
        return CGSize(width: collectionView.visibleSize.height, height: collectionView.visibleSize.height)
    }

}


//
//  ListTableViewController.swift
//  Pina
//
//  Created by SANTIAGO ROJAS HERRERA on 10/4/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchControllerDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var purchasePlanButton: PurchasePlanButton!
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text, !text.isEmpty{
            DataService.instance.getProductsStartingWith(searchValue: text, handler:  { (products) in
                self.searchResults = products
                self.tableView.reloadData()
            })
        }
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        searchResults = nil
        tableView.reloadData()
    }
    
    var searchResults : [Product]?
    
    var listForUser: ListForUser!
    var list: List!
    var products = [ListProduct]()
    
    var searchBar: UISearchBar!
    
    
    func loadList() {
        list = List(ref: DataService.instance.REF_LISTS.child(listForUser.key))
        
        list.ref.child("products").observe(.value) { snapshot in
            var newProducts = [ListProduct]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let product = ListProduct(snapshot: snapshot) {
                    newProducts.append(product)
                }
            }
            self.products = newProducts
            self.purchasePlanButton.isHidden = newProducts.isEmpty
            self.tableView.reloadData()
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        loadList()
        
        navigationItem.title = listForUser.name
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        
        navigationItem.rightBarButtonItem = editButtonItem
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(searchProduct))
        navigationItem.rightBarButtonItems?.append(addButton)
        
        navigationItem.searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController?.delegate = self
        definesPresentationContext = true
        navigationItem.searchController?.obscuresBackgroundDuringPresentation = false
        searchBar = navigationItem.searchController?.searchBar
        searchBar.tintColor = UIColor.white
        searchBar.delegate = self
        searchBar.placeholder = NSLocalizedString("Product View Search Bar", comment: "")
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.setEditing(editing, animated: animated)
    }
    
    @objc func searchProduct() {
        searchBar.becomeFirstResponder()
    }
    
    @objc func addProduct(sender: UIButton) {
        
        guard let searchResults = searchResults else { return }
        let product = searchResults[sender.tag]
        DataService.instance.addProduct(product, toListWithKey: list.key)
        
        let alert = UIAlertController(title: nil, message: NSLocalizedString("Product added to your list", comment: ""), preferredStyle: .alert)
        self.present(alert, animated: true)
        
        let duration = 0.75
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.barTintColor = UIColor(named: "Secundary 300")
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]

        tabBarController?.tabBar.tintColor = UIColor(named: "Secundary 300")
    }
    
    
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let searchResults = searchResults {
            return searchResults.count
        } else {
            return products.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let searchResults = searchResults {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListProductSearchTableViewCell", for: indexPath) as? ListProductSearchTableViewCell else {
                fatalError("The dequeued cell is not an instance of ListTableViewCell.")
            }
            
            cell.productNameLabel?.text = searchResults[indexPath.row].name
            cell.addProductButton.tag = indexPath.row
            cell.addProductButton.addTarget(self, action: #selector(addProduct), for: .touchUpInside)
            return cell
        }
        
        let cellIdentifier = "ListProductTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ListProductTableViewCell else {
            fatalError("The dequeued cell is not an instance of ListTableViewCell.")
        }
        
        let product = products[indexPath.row]
        
        cell.product = product
        
        cell.productNameLabel.text = product.name ?? "LMAO"
        cell.productQuantityLabel.text = String(product.quantity)
        cell.stepper.value = Double(product.quantity)
        
        let imageRef = Storage.storage().reference(withPath: "products/\(product.key)/thumbnail.jpg")
        cell.productImage.sd_setImage(with: imageRef, placeholderImage: UIImage(named: "Product Placeholder"))
        
        return cell
    }
    
    
    // Override to support conditional editing of the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if let _ = searchResults {
            return false
        } else {
            return true
        }

    }
    
    
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let productToDelete = products[indexPath.row]
            list.ref.child("products/\(productToDelete.key)").removeValue()
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier ?? "" {
        case "Show Detail":
            if let cell = sender as? ListProductTableViewCell, let productViewController = segue.destination as? ProductViewController {
                let index = tableView.indexPath(for: cell)?.row ?? 0
                productViewController.productKey = products[index].key
            }
        case "Show Detail from Search":
            if let cell = sender as? ListProductSearchTableViewCell, let productViewController = segue.destination as? ProductViewController {
                let index = tableView.indexPath(for: cell)?.row ?? 0
                productViewController.productKey = searchResults?[index].key
            }
        case "Show Purchase Plan":
            if let purchasePlanViewController = segue.destination as? PurchasePlanViewController {
                purchasePlanViewController.products = products
                purchasePlanViewController.list = list
            }
        default:
            fatalError("Unidentified Segue")
        }
    }
    
}

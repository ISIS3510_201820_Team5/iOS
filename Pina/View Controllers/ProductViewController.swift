//
//  ProductViewController.swift
//  Pina
//
//  Created by SANTIAGO ROJAS HERRERA on 10/3/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import Firebase
import ScrollableGraphView
import Foundation

class ProductViewController: UICollectionViewController {
    
    var productKey: String!
    var product: Product?
    var productImage: UIImage?
    
    
    @IBAction func addProductToList(_ sender: UIBarButtonItem) {
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel) { (action) in
            self.dismiss(animated: true)
        }
        
        let alert = UIAlertController(title: product!.name , message: NSLocalizedString("Add the product to a List", comment: "Add the product to a List") , preferredStyle: .actionSheet)
        alert.addAction(cancelAction)
        
        alert.view.tintColor = UIColor(named: "Secundary 300")
        
        guard let tabBarLists = (tabBarController as? TabBarViewController)?.lists, tabBarLists.count > 0 else {
            
            let alert = UIAlertController(title: nil, message: NSLocalizedString("There are no lists", comment: ""), preferredStyle: .alert)
            self.present(alert, animated: true)
            
            // duration in seconds
            let duration = 0.75
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
                
            }
            return
        }
        
        for list in tabBarLists {
            let action = UIAlertAction(title: list.name, style: .default) { (_) -> Void in
                DataService.instance.addProduct(self.product!, toListWithKey: list.key)
                
                let addedAlert = UIAlertController(title: nil, message: NSLocalizedString("Product added to your list", comment: ""), preferredStyle: .alert)
                self.present(addedAlert, animated: true)
                
                let duration = 0.75
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                    addedAlert.dismiss(animated: true)
                    
                }
            }
            alert.addAction(action)
            
        }
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.barButtonItem = sender
        }
        
        present(alert, animated: true, completion: nil)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageRef = Storage.storage().reference(withPath: "products/\(productKey!)/image.jpg")
        imageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let _ = error {
                print("// Uh-oh, an error occurred!")
            } else {
                if let image = UIImage(data: data!) {
                    self.productImage = image
                    UIView.animate(withDuration: 0.3) {
                        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: image)
                    }
                }
            }
        }

        prepareCollectionView()
        
        DataService.instance.REF_PRODUCTS.child(productKey).observeSingleEvent(of: .value) { (snapshot) in
            self.product = Product(snapshot: snapshot)
            self.navigationItem.title = self.product?.name
            self.collectionView.reloadData()
        }
        
        DataService.instance.REF_PRICES.child(productKey).queryLimited(toLast: 14).observeSingleEvent(of: .value) { (snapshot) in
            self.product?.updatePrices(fromSnapshot: snapshot)
            self.collectionView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
        tabBarController?.tabBar.tintColor = UIColor(named: "Secundary 300")
    }
    
    //MARK: UICollectionViewDataSource
    func prepareCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.alwaysBounceVertical = true
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard kind == UICollectionView.elementKindSectionHeader else { assert(false, "Unexpected element kind") }
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ProductSectionHeaderView.identifier, for: indexPath) as! ProductSectionHeaderView
        switch indexPath.section {
        case 0:
            headerView.sectionTitle.text = NSLocalizedString("Tags", comment: "")
        case 1:
            headerView.sectionTitle.text = NSLocalizedString("Prices", comment: "")
        case 2:
            headerView.sectionTitle.text = NSLocalizedString("Historical Prices", comment: "")
        default:
            headerView.sectionTitle.text = NSLocalizedString("Unrecognized Section", comment: "")
        }
        return headerView
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return product?.tags.count ?? 1
        case 1:
            return product?.prices?.count ?? 1
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            guard product?.tags.count ?? 0 > 0 else { break }
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductTagCell.identifier, for: indexPath) as? ProductTagCell
                else { preconditionFailure("Failed to load collection view cell") }
            cell.tagName = product?.tags[indexPath.item]
            return cell
        case 1:
            guard product?.prices != nil else { break }
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductPriceCell.identifier, for: indexPath) as? ProductPriceCell else { preconditionFailure("Failed to load collection view cell") }
            cell.productPrice = product?.prices?[indexPath.item]
            return cell
        case 2:
            guard product?.historicalPrices.count ?? 0 > 1 else { break }
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductHistoricalPricesCell.identifier, for: indexPath) as? ProductHistoricalPricesCell else { preconditionFailure("Failed to load collection view cell") }
            
            // MARK: Graph Logic
            let graphView = cell.graphView!
            cell.shouldClientBuy = product?.shouldClientBuy()
            graphView.dataSource = self
            return cell
        default:
            break
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NoInformationCell.identifier, for: indexPath) as? NoInformationCell else { preconditionFailure("Failed to load collection view cell") }
        cell.message = "There is no information available"
        return cell

    }
}

extension ProductViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            guard product?.tags.count ?? 0 > 0 else { break }
            guard let tagName = product?.tags[indexPath.item].uppercased() else { return CGSize(width: 40, height: 40)}
            let height = CGFloat(integerLiteral: 40)
            let width = min(tagName.width(withConstrainedHeight: height, font: ProductTagCell.font) + CGFloat(integerLiteral: 24), CGFloat(integerLiteral: 200))
            return CGSize(width: width , height: height)
        case 1:
            guard product?.prices != nil else { break }
            return CGSize(width: view.frame.width, height: 80)
        case 2:
            guard product?.historicalPrices.count ?? 0 > 2 else { break }
            return CGSize(width: view.frame.width, height: 500)
        default:
            break
        }
        return CGSize(width: view.frame.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 64)
    }
    
}

extension ProductViewController: ScrollableGraphViewDataSource {
    func value(forPlot plot: Plot, atIndex pointIndex: Int) -> Double {
        // Return the data for each plot.
        switch(plot.identifier) {
        case "line", "lineDot":
            return product?.historicalPrices[pointIndex].price ?? 0
        default:
            return 0
        }
    }
    
    func label(atIndex pointIndex: Int) -> String {
        guard let date = product?.historicalPrices[pointIndex].date else { return "N/A"}
        print(date)
        return DateFormatter.localizedString(from: date, dateStyle: .medium, timeStyle: .none)
    }
    
    func numberOfPoints() -> Int {
        return product?.historicalPrices.count ?? 0
    }
}

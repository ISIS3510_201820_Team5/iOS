//
//  TabBarViewController.swift
//  Pina
//
//  Created by Santiago Rojas on 10/25/18.
//  Copyright © 2018 Piña. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI

class TabBarViewController: UITabBarController {
    
    var lists : [ListForUser]?
    fileprivate(set) var auth: Auth?
    fileprivate(set) var authUI: FUIAuth?
    
    let reachability = Reachability()!
    
    var connectionBanner: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        auth = Auth.auth()
        authUI = FUIAuth.defaultAuthUI()
        authUI?.shouldHideCancelButton = true
        authUI?.delegate = self
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user == nil {
                let authViewController = self.authUI?.authViewController()
                authViewController?.modalPresentationStyle = .formSheet
                self.present(authViewController!, animated: true)
                self.lists = nil
            } else {
                DataService.instance.loadLists(handler: { (lists) in
                    self.lists = lists
                })
            }
        }
        
        connectionBanner = setNoConnectionBanner()
        
        reachability.whenReachable = { _ in
            UIView.animate(withDuration: 0.3, animations: {
                self.connectionBanner.alpha = 0
            }) { (finished) in
                self.connectionBanner.isHidden = finished
            }
        }
        reachability.whenUnreachable = { _ in
            self.connectionBanner.alpha = 0
            self.connectionBanner.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.connectionBanner.alpha = 1
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
    }
    
    func setNoConnectionBanner() -> UIView {
        let containerView = UIView()
        containerView.isHidden = true
        containerView.backgroundColor = #colorLiteral(red: 0.8431372643, green: 0.2274509817, blue: 0.2862745225, alpha: 1)
        view.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        containerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: tabBar.topAnchor).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 14).isActive = true
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.text = NSLocalizedString("No internet connection", comment: "No internet connection")
        containerView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        label.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        label.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        
        return containerView
    }
    
}

extension TabBarViewController: FUIAuthDelegate {
    func emailEntryViewController(forAuthUI authUI: FUIAuth) -> FUIEmailEntryViewController {
        return EmailEntryViewController(nibName: "EmailEntryViewController", bundle: Bundle.main, authUI: authUI)
    }
    
    func passwordSignInViewController(forAuthUI authUI: FUIAuth, email: String) -> FUIPasswordSignInViewController {
        return PasswordSignInViewController(nibName: "PasswordSignInViewController", bundle: Bundle.main, authUI: authUI, email: email)
    }
    
    func passwordSignUpViewController(forAuthUI authUI: FUIAuth, email: String) -> FUIPasswordSignUpViewController {
        let passwordSignUpViewController = FUIPasswordSignUpViewController(authUI: authUI, email: email)
        passwordSignUpViewController.view.backgroundColor = UIColor(named: "Surface")
        passwordSignUpViewController.view.tintColor = UIColor(named: "Secundary 400")
        return passwordSignUpViewController
    }
}
